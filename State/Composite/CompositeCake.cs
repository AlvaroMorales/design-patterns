﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite
{
    class CompositeCake : Component
    {
        private List<Component> ingredients = new List<Component>();
        public decimal Cost
        {
            get
            {
                decimal cost = 0;
                ingredients.ForEach(x =>
                {
                    if (x.GetType().Name.Equals(this.GetType().Name))
                        cost += ((CompositeCake)x).Cost;
                    else
                        cost += x.Price;
                });
                return cost;
            }
        }
        public void Add(Component componentIngredient) => this.ingredients.Add(componentIngredient);
        public void Remove(Component componentIngredient) => this.ingredients.Remove(componentIngredient);

        public CompositeCake(string name,
            decimal price = 0) : base(name, price) { }

    }
}
