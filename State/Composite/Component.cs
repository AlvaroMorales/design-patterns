﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite
{
    abstract class Component
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Component(string name, decimal price)
        {
            this.Name = name;
            this.Price = price;
        }

    }
}
