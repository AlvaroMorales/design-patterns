﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite
{
    class Ingredient : Component
    {
        public int Quantity { get; set; }
        public int Unit { get; set; }
        public Ingredient(string name, 
            decimal price, 
            int quantity,
            int unit) : base(name, price)
        {
            this.Quantity = quantity;
            this.Unit = unit;
        }
    }
}
