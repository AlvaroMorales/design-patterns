﻿using System;

namespace Composite
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Ingredient ingredient1 = new Ingredient("Chips", 100, 10, 1);
            Ingredient ingredient2 = new Ingredient("Milk", 40, 12, 2);
            Ingredient ingredient3 = new Ingredient("Eggs", 16, 4, 3);

            CompositeCake cheeseCake = new CompositeCake("CheeseCake");
            cheeseCake.Add(ingredient1);
            cheeseCake.Add(ingredient2);
            cheeseCake.Add(ingredient3);

            Console.WriteLine(cheeseCake.Cost);

            Ingredient ingredient4 = new Ingredient("Chocolate",123,3,4);
            CompositeCake chocolateCake = new CompositeCake("Chocolate");
            
            chocolateCake.Add(ingredient4);
            chocolateCake.Add(cheeseCake);
       
            Console.WriteLine(chocolateCake.Cost);

        }
    }
}
