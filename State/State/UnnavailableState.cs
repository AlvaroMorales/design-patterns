﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class UnnavailableState : ServerState
    {
        public override void Response()
        {
            Task.Delay(500);
            Console.WriteLine("Status 200 with 500 of delay.");
        }
    }
}
