﻿using System;

namespace State
{
    public class Program
    {
        static void Main(string[] args)
        {
            ContextServer contextServer = new ContextServer();
            contextServer.state = new ServerStateAvailable();

            contextServer.AttendRequest();

            contextServer.state = new UnnavailableState();
            contextServer.AttendRequest();
            contextServer.AttendRequest();

            contextServer.state = new SuperUnnavailableState();
            contextServer.AttendRequest();
            contextServer.AttendRequest();


            contextServer.state = new BrokenServerState();
            contextServer.AttendRequest();
            contextServer.AttendRequest();

        }
    }
}
