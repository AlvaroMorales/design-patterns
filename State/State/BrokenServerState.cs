﻿using System;
using System.Collections.Generic;
using System.Text;

namespace State
{
    public class BrokenServerState : ServerState
    {
        public override void Response()
        {
            Console.WriteLine("Response 503");
        }
    }
}
