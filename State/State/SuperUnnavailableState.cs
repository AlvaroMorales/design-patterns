﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class SuperUnnavailableState : ServerState
    {
        public override void Response()
        {
            Task.Delay(1000);
            Console.WriteLine("Status 200");
        }
    }
}
