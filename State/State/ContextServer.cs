﻿using System;
using System.Collections.Generic;
using System.Text;

namespace State
{
    public class ContextServer
    {
        public ServerState state { get; set; }
        public void AttendRequest()
        {
            this.state.Response();
        }
        public virtual void test() { string s = ""; }
    }
}
