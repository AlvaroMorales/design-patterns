﻿using Mediator.Colleages;
using System;

namespace Mediator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mediator mediator = new Mediator();

            Colleage userNotifier = new User(mediator);

            Colleage user = new User(mediator);
            Colleage userAdmin = new UserAdmin(mediator);

            mediator.Add(user);
            mediator.Add(userAdmin);

            userNotifier.Communicate("Something happened...!");

            user.Communicate("Something could've happened...!");

        }
    }
}
