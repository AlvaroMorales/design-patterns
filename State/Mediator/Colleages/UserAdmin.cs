﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediator.Colleages
{
    public class UserAdmin : Colleage
    {
        public UserAdmin(IMediator mediator) : base(mediator) { }
        public override void RecieveMessage(string message) =>
            Console.WriteLine($"An user admin just recieved {message}");
    }
}
