﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediator.Colleages
{
    public class User : Colleage
    {
        public User(IMediator mediator) : base(mediator) { }
        public override void RecieveMessage(string message) =>
            Console.WriteLine($"An user just recieved {message}");

    }
}
