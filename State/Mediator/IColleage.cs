﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediator
{
    public abstract class Colleage
    {
        private IMediator _mediator;
        public IMediator _Mediator { get; }
        public Colleage(IMediator mediator)
        {
            this._mediator = mediator;
        }
        public virtual void Communicate(string message)
        {
            this._mediator.Send(message, this);
        }
        public abstract void RecieveMessage(string message);
    }
}
