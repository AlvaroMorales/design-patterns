﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mediator
{
    public class Mediator : IMediator
    {
        private List<Colleage> colleages;
        public Mediator() => this.colleages = new List<Colleage>();
        public void Add(Colleage colleage) => this.colleages.Add(colleage);

        public void Send(string message, Colleage colleage)
        {
            foreach (var col in this.colleages)
            {
                if (col != colleage)
                {
                    col.RecieveMessage(message);
                }
            }
        }
    }
}
